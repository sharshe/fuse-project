package com.kscl.esbatcs;

import java.io.IOException;

import org.apache.camel.Exchange;
import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;

public class Filter {

	public void getFormData(Exchange exchange) throws UnsupportedOperationException, IOException{
		
		MultipartEntityBuilder entity = MultipartEntityBuilder.create();
		entity.addTextBody("licence", "ccT0F804bU8093N3900I5ff4Sfe49Ob58aS3b");
		entity.addTextBody("client", "KSCL");
		entity.addTextBody("SystemCodeNumber", exchange.getIn().getHeader("SystemCodeNumber").toString());
		
		HttpEntity resultEntity = entity.build();
		exchange.getOut().setHeader(Exchange.CONTENT_TYPE, resultEntity.getContentType().getValue());
		exchange.getOut().setBody(resultEntity.getContent());
		
		//exchange.getOut().setHeader(Exchange.CONTENT_TYPE, entity.build().getContentType());
		//exchange.getOut().setBody(entity.build().getContent());
		
	}

}
